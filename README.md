# pr2dim

Prometheus to DIM bridge

## Getting started

Python3, pydim and requests are required.
The process is simply started on the command-line. There is a command-line help. Per default it will fork into the background and will use ecs04 as DIM_DNS_NODE. 

## Development hints

Currently the parsing is done mostly by hand, i.e. for each metric a dedicated call-back must be implemented. 

## Docker

Building the docker image will be taken care of by gitlab ci.
Pull the image:
```bash
    docker pull gitlab-registry.cern.ch/lhcb-online/pr2dim:latest
```

In case you want to build it manually

```bash
    docker build -t pr2dim:latest .
```