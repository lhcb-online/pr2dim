#!/usr/bin/env python3
#
# pr2dim: simple-minded Prometheus to DIM bridge
#
import requests
import argparse
import signal
import os
import sys
import logging
import logging.handlers
from pydim import dis_add_service, dis_start_serving, dis_update_service, dis_set_dns_node
from time import sleep

POLLING_INTERVAL = 10 # seconds
DIM_DNS_NODE = 'ecs04'

# Query data.
# Array of dictionaries
# (url, metric == DIM SERVICE NAME, DIM callback function)
# URL is the complete URL for the query - can be pasted in curl
# All DIM Services are strings
# Arrays are reported separated by ';'
# Paramters are separated by ' '
# Spaces are replaced by _
# Global polling rate
# DIM Service update queries prometheus and sends answer

def dim_cb_eb_pcie_width(index):
    #logging.debug(index) 
    try:
        r = requests.get(queries[index]['url']).json()
    except: 
        logging.debug("Failed to retrieve %s" % queries[index]['url'])
        return ('ERROR_PROMETHEUS_READ', )
    #logging.debug(r)
    if r['status'] != 'success':
        return ('ERROR_PROMETHEUS_READ', )
    data = r['data']['result']
    result = ''
    for d in data:
        if not 'metric' in d:
            continue
        m = d['metric']
        s = m.get('host', 'HOST_UNKNOWN') + ',' + m.get('model', 'MODEL_UNKNOWN') + ',' + m.get('slot', 'SLOT_UNKNOWN')
        result = result + s.upper() + ';'
    # build answer, return as tuple
    #logging.debug(result)
    if result == '':
        return ('NONE', )
    else:
        return (result[:-1],)

# Declaration of metrics & callbacks
queries = [
    { 'url' : 'http://ebprometheus.lbdaq.cern.ch:9090/api/v1/query?query=max_over_time(eventbuilder_pcie_status_degraded_width[15m]) == 1',
      'metric' : 'eventbuilder_pcie_status_degraded_width',
      'dim_cb' : dim_cb_eb_pcie_width } 
]

# List of DIM Services
services = []

def setup():
    # create DIM services
    global POLLING_INTERVAL
    for i, query in enumerate(queries):
        s = dis_add_service('/PROMETHEUS/%s' % query['metric'].upper(), 'C', query['dim_cb'], i)
        if s == -1:
            logging.error('Could not register service for metric %' % query['metric'])
            continue
        else:
            services.append(s)
    # set parameters
    if 'P2D_POLLING_INTERVAL' in os.environ.keys():
        POLLING_INTERVAL = int(os.environ['P2D_POLLING_INTERVAL'])
    if not 'DIM_DNS_NODE' in os.environ.keys():
        logging.info('Setting DIM DNS to %s' % DIM_DNS_NODE)
        dis_set_dns_node(DIM_DNS_NODE)

def loop():
    while True:
        for s in services:
            dis_update_service(s)
        sleep(POLLING_INTERVAL)
    # never returns
  
def main():
    parser = argparse.ArgumentParser(description='Relay Prometheus metrics to DIM')
    parser.add_argument('--foreground', '-f', dest='foreground', action='store_true', default=False, help='run in the foreground')
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    if args.foreground:
        setup()
        dis_start_serving('PROMETHEUS') 
        loop()
    else:
        signal.signal(signal.SIGTTOU, signal.SIG_IGN)
        signal.signal(signal.SIGTTIN, signal.SIG_IGN)
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
        os.setpgrp()
        signal.signal(signal.SIGHUP, signal.SIG_IGN)
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
        sys.stdin.close()
        sys.stdout.close()
        sys.stderr.close()
        os.umask(000)
        signal.signal(signal.SIGCLD, signal.SIG_IGN)
        handler = logging.handlers.SysLogHandler()
        # when we are 'daemonic' we need to close the stdout logging handler (otherwise we crash)
        logging.getLogger().root.handlers.clear()
        logging.getLogger().addHandler(handler)
        setup()
        dis_start_serving('PROMETHEUS')
        loop()

if __name__ == '__main__':
    main()

    
