FROM python:3.7.13-slim-buster

RUN mkdir -p /app
WORKDIR /app
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

ADD pr2dim.py /app/pr2dim.py

CMD [ "python3", "pr2dim.py", "-f"]